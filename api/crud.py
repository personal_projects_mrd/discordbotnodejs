from sqlalchemy.orm import Session

# from . import models, schemas

from models import Server, Op
from schemas import ServerSchema, ServerSchemaCreate, OpSchema, OpSchemaCreate

# def get_ops(db: Session, server_id: str):
#


def crud_add_server(db: Session, server: ServerSchemaCreate):
    db_server = Server(id=server.id)
    db.add(db_server)
    db.commit()
    db.refresh(db_server)
    return db_server


def crud_get_servers(db: Session, skip: int = 0, limit: int = 10):
    return db.query(Server).offset(skip).limit(limit).all()


def crud_get_server(db: Session, server_id: int):
    return db.query(Server).filter(Server.id == server_id).first()


def crud_get_server_bdid(db: Session, server_bdid: int):
    return db.query(Server).filter(Server.bdid == server_bdid).first()
