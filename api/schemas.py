from typing import List, Optional

from pydantic import BaseModel


class OpSchema(BaseModel):
    bdid: int
    id: str
    server_id: str

    class Config:
        orm_mode = True


class OpSchemaCreate(BaseModel):
    id: str
    server_id: str


class ServerSchema(BaseModel):
    bdid: int
    id: str
    ops: List[OpSchema] = []

    class Config:
        orm_mode = True


class ServerSchemaCreate(BaseModel):
    id: str
