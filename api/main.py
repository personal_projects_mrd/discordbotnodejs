from typing import List

from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session

# from . import crud, models, schemas
from crud import crud_add_server, crud_get_servers, crud_get_server, crud_get_server_bdid
from models import Server, Op
from schemas import ServerSchema, ServerSchemaCreate, OpSchema, OpSchemaCreate
from database import SessionLocal, engine, Base

Base.metadata.create_all(bind=engine)

app = FastAPI()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get("/")
async def read_main():
    return {"msg": "Hello World"}


@app.get("/servers/", response_model=List[ServerSchema])
def read_servers(db: Session = Depends(get_db)):
    return crud_get_servers(db=db)


@app.post("/server/", response_model=ServerSchema)
def add_server(server: ServerSchemaCreate, db: Session = Depends(get_db)):
    db_server = crud_get_server(db, server_id=server.id)
    if db_server:
        raise HTTPException(status_code=400, detail="Server already exists.")
    return crud_add_server(db=db, server=server)


@app.get("/server/{server_id}/", response_model=ServerSchema)
def read_server(server_id: str, db: Session = Depends(get_db)):
    db_server = crud_get_server(db=db, server_id=server_id)
    if db_server is None:
        raise HTTPException(status_code=404, detail="Server not found")
    return db_server

@app.get("/server/bdid/{server_bdid}/", response_model=ServerSchema)
def read_server(server_bdid: int, db: Session = Depends(get_db)):
    db_server = crud_get_server_bdid(db=db, server_bdid=server_bdid)
    if db_server is None:
        raise HTTPException(status_code=404, detail="Server not found")
    return db_server
