module.exports = {
    name: 'ping',
    description: 'Ping Fastapi',
    args: true,

    execute(message, args) {
    const fetch = require('node-fetch');
    if (args[0] == 'json'){
        fetch('http://localhost:8000/servers/')
        .then(res => res.json())
        .then(json => message.channel.send(json));
    } else if (args[0] == 'text'){
        fetch('http://localhost:8000/servers/')
        .then(res => res.text())
        .then(text => message.channel.send(text));
    }
    }
}
